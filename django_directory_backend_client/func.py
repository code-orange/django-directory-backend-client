import requests
import os


def directory_backend_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "DIRECTORY_BACKEND_API_URL",
            default="https://directory-backend-api.example.com/",
        )

    return settings.DIRECTORY_BACKEND_API_URL


def directory_backend_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("DIRECTORY_BACKEND_API_USER", default="user")
        password = os.getenv("DIRECTORY_BACKEND_API_PASSWD", default="secret")
        return username, password

    username = settings.DIRECTORY_BACKEND_API_USER
    password = settings.DIRECTORY_BACKEND_API_PASSWD

    return username, password


def directory_backend_head(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.head(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def directory_backend_get_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.get(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def directory_backend_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def directory_backend_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def directory_backend_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def directory_backend_delete_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{directory_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def directory_backend_get_api_credentials_for_customer(mdat_id: int):
    username, password = directory_backend_default_credentials()

    if username != mdat_id:
        response = directory_backend_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def directory_backend_get_tenants(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/tenant",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def directory_backend_get_tenant(mdat_id: int, org_tag: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        f"v1/tenant/{org_tag}",
        username,
        password,
    )

    return response


def directory_backend_get_users(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/user",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def directory_backend_get_user(mdat_id: int, key: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        f"v1/user/{key}",
        username,
        password,
    )

    return response


def directory_backend_create_user(mdat_id: int, user_data: dict):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_post_data(
        "v1/user",
        username,
        password,
        data=user_data,
    )

    return response


def directory_backend_delete_user(mdat_id: int, key: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_delete_data(
        f"v1/user/{key}",
        username,
        password,
    )

    return response


def directory_backend_change_user_password(
    mdat_id: int, key: str, new_password: str, old_password: str = None
):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    data = {
        "key": key,
        "new_password": new_password,
    }

    if old_password:
        data["old_password"] = old_password

    response = directory_backend_post_data(
        "v1/userpassword",
        username,
        password,
        data=data,
    )

    return response


def directory_backend_get_groups(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/group",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def directory_backend_get_group(mdat_id: int, group_sid: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        f"v1/group/{group_sid}",
        username,
        password,
    )

    return response


def directory_backend_create_group(mdat_id: int, group_name: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_post_data(
        "v1/group",
        username,
        password,
        data={
            "group_name": group_name,
        },
    )

    return response


def directory_backend_delete_group(mdat_id: int, group_sid: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_delete_data(
        f"v1/group/{group_sid}",
        username,
        password,
    )

    return response


def directory_backend_get_computers(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/computer",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def directory_backend_get_computer(mdat_id: int, computer_sid: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        f"v1/computer/{computer_sid}",
        username,
        password,
    )

    return response


def directory_backend_create_computer(mdat_id: int, computer_type: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_post_data(
        "v1/computer",
        username,
        password,
        data={
            "computer_type": computer_type,
        },
    )

    return response


def directory_backend_delete_computer(mdat_id: int, computer_sid: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_delete_data(
        f"v1/computer/{computer_sid}",
        username,
        password,
    )

    return response


def directory_backend_get_computerjoin(mdat_id: int, computer_sid: str):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        f"v1/computerjoin/{computer_sid}",
        username,
        password,
    )

    return response


def directory_backend_get_static_values(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/directorybackendstatic",
        username,
        password,
    )

    if "objects" not in response:
        return dict()

    static_values = dict()

    for attribute in response["objects"]:
        static_values[attribute["attribute"]] = attribute["value"]

    return static_values


def directory_backend_get_authdomain(mdat_id: int):
    username, password = directory_backend_get_api_credentials_for_customer(mdat_id)

    response = directory_backend_get_data(
        "v1/authdomain",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]
